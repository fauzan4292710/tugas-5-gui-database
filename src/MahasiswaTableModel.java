/*
 * Fauzan Raditya Irawan
 * 235150401111069
 * Sistem Informasi - E
 */

import javax.swing.table.DefaultTableModel;

public class MahasiswaTableModel extends DefaultTableModel {
    public MahasiswaTableModel() {
        super(new String[] { "NIM", "Nama", "Program Studi", "Fakultas" }, 0);
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        // Membuat cell tidak dapat diedit
        return false;
    }
}

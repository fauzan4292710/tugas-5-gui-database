/*
 * Fauzan Raditya Irawan
 * 235150401111069
 * Sistem Informasi - E
 */

import javax.swing.*;
import java.awt.*;

public class InputPanel extends JPanel {
    private JTextField nimField;
    private JTextField namaField;
    private JTextField programStudiField;
    private JTextField fakultasField;

    private JTable table;

    private DatabaseConnection dbConnection;
    private MahasiswaTableModel model;

    public InputPanel(DatabaseConnection dbConnection, MahasiswaTableModel model, JTable table) {
        this.dbConnection = dbConnection;
        this.model = model;
        this.table = table;

        setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(5, 5, 5, 5);
        gbc.anchor = GridBagConstraints.WEST;

        // NIM
        gbc.gridx = 0;
        gbc.gridy = 0;
        add(new JLabel("NIM:"), gbc);

        gbc.gridx = 1;
        gbc.gridy = 0;
        nimField = new JTextField(15);
        add(nimField, gbc);

        // Nama
        gbc.gridx = 0;
        gbc.gridy = 1;
        add(new JLabel("Nama:"), gbc);

        gbc.gridx = 1;
        gbc.gridy = 1;
        namaField = new JTextField(15);
        add(namaField, gbc);

        // Program Studi
        gbc.gridx = 0;
        gbc.gridy = 2;
        add(new JLabel("Program Studi:"), gbc);

        gbc.gridx = 1;
        gbc.gridy = 2;
        programStudiField = new JTextField(15);
        add(programStudiField, gbc);

        // Fakultas
        gbc.gridx = 0;
        gbc.gridy = 3;
        add(new JLabel("Fakultas:"), gbc);

        gbc.gridx = 1;
        gbc.gridy = 3;
        fakultasField = new JTextField(15);
        add(fakultasField, gbc);

        // Tambah Data Button
        gbc.gridx = 0;
        gbc.gridy = 4;
        gbc.gridwidth = 2;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        JButton addButton = new JButton("Tambah Data");

        addButton.addActionListener(e -> {
            String nim = nimField.getText();
            String nama = namaField.getText();
            String programStudi = programStudiField.getText();
            String fakultas = fakultasField.getText();

            // Validasi agar semua bidang input tidak boleh kosong
            if (nim.isEmpty() || nama.isEmpty() || programStudi.isEmpty() || fakultas.isEmpty()) {
                JOptionPane.showMessageDialog(this, "Semua bidang input harus diisi.", "Peringatan",
                        JOptionPane.WARNING_MESSAGE);
                return; // Hentikan proses tambah data jika ada bidang input yang kosong
            }

            // Tambahkan data ke database
            dbConnection.addData(nim, nama, programStudi, fakultas, this);

            // Bersihkan tabel dan isi ulang data dari database
            model.setRowCount(0);
            dbConnection.fillTableModel(model);

            // Bersihkan field input
            clearFields();
        });
        add(addButton, gbc);

        // Hapus Data Button
        gbc.gridx = 0;
        gbc.gridy = 5;
        gbc.gridwidth = 2;
        JButton deleteButton = new JButton("Hapus Data Terpilih");
        deleteButton.addActionListener(e -> {
            int selectedRow = table.getSelectedRow();
            if (selectedRow != -1) {
                String nim = (String) table.getValueAt(selectedRow, 0);
                dbConnection.deleteData(nim, this);
                model.removeRow(selectedRow);
            } else {
                JOptionPane.showMessageDialog(this, "Pilih baris terlebih dahulu.", "Peringatan",
                        JOptionPane.WARNING_MESSAGE);
            }
        });
        add(deleteButton, gbc);
    }

    private void clearFields() {
        nimField.setText("");
        namaField.setText("");
        programStudiField.setText("");
        fakultasField.setText("");
    }
}

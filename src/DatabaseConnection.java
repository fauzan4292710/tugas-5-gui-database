/*
 * Fauzan Raditya Irawan
 * 235150401111069
 * Sistem Informasi - E
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class DatabaseConnection {
    static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost/data_mahasiswa_pemlan";
    static final String USER = "root";
    static final String PASS = "10agustus";

    public void fillTableModel(MahasiswaTableModel model) {
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        model.setRowCount(0);

        try {
            // Register driver yang akan dipakai
            Class.forName(JDBC_DRIVER);

            // Buat koneksi ke database
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            // Buat objek statement
            stmt = conn.createStatement();

            // Buat query ke database
            String sql = "SELECT * FROM data_mahasiswa";

            // Eksekusi query dan simpan hasilnya di obj ResultSet
            rs = stmt.executeQuery(sql);

            // Tampilkan hasil query ke dalam table
            while (rs.next()) {
                String nim = rs.getString("nim");
                String nama = rs.getString("nama");
                String programStudi = rs.getString("program_studi");
                String fakultas = rs.getString("fakultas");
                model.addRow(new Object[] { nim, nama, programStudi, fakultas });
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // Menutup semua koneksi
            try {
                if (rs != null)
                    rs.close();
                if (stmt != null)
                    stmt.close();
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void addData(String nim, String nama, String programStudi, String fakultas, JPanel parentPanel) {
        Connection conn = null;
        Statement stmt = null;

        try {
            // Register driver yang akan dipakai
            Class.forName(JDBC_DRIVER);

            // Buat koneksi ke database
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            // Buat objek statement
            stmt = conn.createStatement();

            // Buat query untuk menambahkan data
            String sql = String.format(
                    "INSERT INTO data_mahasiswa (nim, nama, program_studi, fakultas) VALUES ('%s', '%s', '%s', '%s')",
                    nim, nama, programStudi, fakultas);

            // Eksekusi query
            stmt.executeUpdate(sql);

            // Notifikasi bahwa data berhasil dimasukkan
            JOptionPane.showMessageDialog(parentPanel, "Data berhasil dimasukkan ke dalam database.", "Sukses",
                    JOptionPane.INFORMATION_MESSAGE);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // Menutup semua koneksi
            try {
                if (stmt != null)
                    stmt.close();
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void deleteData(String nim, JPanel parentPanel) {
        Connection conn = null;
        Statement stmt = null;

        try {
            // Register driver
            Class.forName(JDBC_DRIVER);

            // Buat koneksi ke database
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            // Buat objek statement
            stmt = conn.createStatement();

            // Buat query untuk menghapus data
            String sql = "DELETE FROM data_mahasiswa WHERE nim='" + nim + "'";

            // Eksekusi query
            stmt.executeUpdate(sql);

            JOptionPane.showMessageDialog(
                    parentPanel, "Data berhasil dihapus dari database.", "Sukses",
                    JOptionPane.INFORMATION_MESSAGE);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // Menutup semua koneksi
            try {
                if (stmt != null)
                    stmt.close();
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}

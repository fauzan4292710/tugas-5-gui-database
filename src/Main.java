/*
 * Fauzan Raditya Irawan
 * 235150401111069
 * Sistem Informasi - E
 */

import javax.swing.*;
import java.awt.BorderLayout;

public class Main {
    public static void main(String[] args) {
        // Membuat frame untuk GUI
        JFrame frame = new JFrame("Data Mahasiswa");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(800, 600);

        // Membuat instance dari MahasiswaTableModel
        MahasiswaTableModel model = new MahasiswaTableModel();

        // Mengisi data model dari database
        DatabaseConnection dbConnection = new DatabaseConnection();
        dbConnection.fillTableModel(model);

        // Membuat table dan menambahkan model
        JTable table = new JTable(model);

        // Menambahkan table ke dalam scroll pane
        JScrollPane scrollPane = new JScrollPane(table);
        frame.add(scrollPane, BorderLayout.CENTER);

        // Membuat panel untuk form input data
        InputPanel inputPanel = new InputPanel(dbConnection, model, table);
        frame.add(inputPanel, BorderLayout.SOUTH);

        // Menampilkan frame
        frame.setVisible(true);
    }
}
